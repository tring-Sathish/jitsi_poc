import React from 'react';
import logo from './logo.svg';
import './App.css';
import JitsiComponent from './jitsi'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <JitsiComponent/>
      </header>
    </div>
  );
}

export default App;
