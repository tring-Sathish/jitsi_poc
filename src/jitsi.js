import React, { useState } from 'react'
import Jitsi from 'react-jitsi'
// import Loader from './components/Loader'
 
const JitsiComponent = () => {
 
    const [displayName, setDisplayName] = useState('')
    const [roomName, setRoomName] = useState('')
    const [password, setPassword] = useState('')
    const [onCall, setOnCall] = useState(false)
 
    return onCall
        ? (
            <Jitsi
                containerStyle={{ width: '1200px', height: '650px' }}
                interfaceConfig={{ SHOW_BRAND_WATERMARK: false }}
                config={{ enableClosePage: true }}
                roomName={roomName}
                domain='http://tringchat.tringbytes.com/'
                displayName={displayName}
                password={password}
                // loadingComponent={Loader}
                onAPILoad={JitsiMeetAPI => console.log('Good Morning everyone!')}
            />)
        : (
            <>
                <h1>Create a Meeting</h1>
                <input type='text' placeholder='Room name' value={roomName} onChange={e => setRoomName(e.target.value)} />
                <input type='text' placeholder='Your name' value={displayName} onChange={e => setDisplayName(e.target.value)} />
                <input type='password' placeholder='room password' value={password} onChange={e => setPassword(e.target.value)} />
                <button onClick={() => setOnCall(true)}> Let&apos;s start!</button>
            </>
        )
 
}
 
export default JitsiComponent